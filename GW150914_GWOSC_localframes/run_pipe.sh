#!/bin/bash -e
LABEL=GW150914_GWOSC
TRIGTIME=1126259462.42
BAYESWAVE_PIPE=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/bayeswave_pipe
LIGO_DATAFIND_SERVER=datafind.gw-openscience.org

for deploy in conda singularity; do 
    config=${LABEL}_${deploy}.ini
    workdir=$(echo ${config} | sed -e "s/.ini//g")

    ${BAYESWAVE_PIPE} ${config} \
        --workdir ${workdir} \
        --server ${LIGO_DATAFIND_SERVER} \
        --skip-megapy \
        --trigger-time ${TRIGTIME}

    # Remove x509 proxy and use relative paths for portability
    sed -i "/^x509*/d" ${workdir}/*.sub
    sed -i "/^getenv*/d" ${workdir}/*.sub
    rm ${workdir}/x509up*
    sed -i "s|${PWD}/${workdir}/||g" ${workdir}/*.sub
    sed -i "s|${PWD}/||g" ${workdir}/${workdir}.dag
done
