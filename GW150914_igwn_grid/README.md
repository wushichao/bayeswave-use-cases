# GW15091
Sets up a simple standard analysis of GW150914 using Hanford & Livingston
authenticated CVMFS data using IGWN Grid resources (aka OSG).

`run_pipe.sh` generates the workflow using the current release of
`bayeswave_pipe` packaged by conda in the LIGO CVMFS oasis. Two workflows are generated:
 * `GW150914_IGWN_conda`: executes `BayesWave` and `BayesWavePost` via the
   BayesWave conda distribution in LIGO's CVMFS oasis.
 * `GW150914_IGWN_singularity`: executes `BayesWave` and `BayesWavePost` via the
   singularity container in the OSG CVMFS singularity repository.

**For convenience, both entire workflows are included in this repository.**

To submit:
```
$ condor_submit_dag GW150914_IGWN_conda/GW150914_IGWN_conda.dag
```
and similarly for the singularity version.

The analysis/workflow structure is trivial:
 * A single (parent) `BayesWave` job reads (open) LIGO strain data from CVMFS
   and samples the posterior probability density functions for the spectral,
   signal and noise models via an RJMCMC algorithm. 
 * A single (child) `BayesWavePost` job reads the posterior samples from
   `BayesWave` and reconstructs the time- and-frequency domain representations
   of the corresponding waveforms.

Note that the `BayesWave` job has been configured to only run for 10000 samples
(the production default is 4000000) in order that the job will complete in a few
minutes (`BayesWavePost` should complete in ~seconds).  To run a physically
meaningful analysis, which should take ~12 *hours* (with self-checkpointing),
modify the `bayeswave.sub` files:

From:

```
 --Niter 10000
```
   
To:

```
 --Niter 4000000
```

Alternatively, edit the `.ini` files appropriately and rerun `run_pipe`
